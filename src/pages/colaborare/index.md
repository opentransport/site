---
title: Alăturați-vă
panels:
  - title: Alăturați-vă OpenTransport
    body: OpenTransport este un pachet de servicii deținut și dezvoltat de OpenTransport pentru rutarea transportului public. Este posibil ca municipalitățile să se alăture serviciului.
    links: []
    image: "../municipalities.svg"
    id: municipalities
    textColor: "#000"
    swapped: true
---

## Procesul de inregistrare

Vă rugăm să trimiteți informațiile de mai jos la hello@opentransport.ro.

### 1. De acord cu cooperarea cu OpenTransport

De acord cu folosirea serviciilor propuse de OpenTransport.

### 2. Denumiți managerul responsabil de aceste servicii pentru organizația dvs.

Denumiți managerul responsabil de aceste servicii pentru organizația dvs. El este responsabil pentru comunicarea operațională si setarea serviciului Open Transport și este persoana noastră de contact în caz de probleme.

### 3. Furnizați informații de contact pentru managerul de servicii

Informatii necesare manager servicii:

- Nume
- E-mail
- Număr de telefon

### 4. Numele de domeniu

Traficul către OpenTransport este întotdeauna https criptat. Puteți alege numele de domeniu al serviciului dvs. (de exemplu, https://reittiopas.kaupunki.fi) și avem nevoie de certificarea TLS pe care trebuie să o obțineți. Puteți utiliza, de asemenea, https://kaupunki.digitransit.fi, unde nu aveți nevoie de un certificat separat. Date necesare:

- Nume de domeniu
- Certificare TLS dacă doriți propriul nume de domeniu

### 5. Date despre reatea transport

Serviciul OpenTransport integrează datele dvs. de rută în format GTFS. De asemenea, este posibilă preluarea informațiilor din sisteme conexe. imagine:

- Adresa serviciului web de pe care este disponibil pachetul GTFS

Asigurați-vă că datele din pachetul GTFS sunt în regulă folosind, de exemplu, validatorul Google GTFS:

- Descărcați GTFS
- Descărcați https://github.com/google/transitfeed/releases/latest și dezarhivați
- Accesați folderul validatorului și executați `./feedvalidator.py -m <calea gtfs.zip>`

### 6. Moduri de acces

![](./images/kulkumuodot.png)

Specificați modurile de transport găsite în datele dvs. de rută. Modurile de transport selectate permit utilizatorului să limiteze cantitatea de trafic disponibilă. Descrieți nevoia pentru fiecare dintre următoarele moduri de transport:

- Autobuzul
- Trenul
- Metrou
- Tramvai
- Avion
- Vaporas
- Biciclete urbane

### 7. Zona de căutare

![](./images/hakualue.png)

Setați limitele hărții pentru căutarea adreselor. OpenTransport configurează o zonă dreptunghiulară pentru a căuta adresele introduse de utilizator. imagine:

- Indicați în stânga sus în coordonatele WGS84
- Indicați în dreapta jos în coordonatele WGS84

### 8. Locatii implicite

![](./images/lahtopaikka.png)

Interfața OpenTransport încearcă întotdeauna să localizeze utilizatorul. Dacă poziționarea nu are succes, este selectat punctul specificat ca punct de pornire

- Coordonatele punctului de plecare implicit (WGS84) și numele locului în cauză.

