﻿---
title: Roadmap
panels:
  - title: Directii de dezvoltare
    body: Ne dorim mereu să crestem calitatea serviciilor oferite, investim în tehnologie și idei inovatoare. În cele ce urmează o să găsești o scurtă prezentare a planurilor și ideloar noastre. Acestea posibil să fie doar o parte din ideile ce le considerăm importante pentru a fi realizate în urmatoarea perioadă.
    links: []
    image: "../roadmap.svg"
    id: roadmap
    textColor: "#000"
    swapped: true
---

### Obiective:

**Managementul perturbărilor și al excepțiilor** <br>
Dezvoltarea OpenTransport pentru a prezenta clar întreruperi de trafic și situații de urgență, cum ar fi schimburi anulate și excepții de șantier. Sunt investigate diferite situații anormale, cazuri de utilizare și date de referință. Inițial, o implementare cuprinzătoare pentru alertele de service GTFS-RT și actualizarea călătoriei.

**Management API** <br>
Monitorizarea lățimii de bandă, posibile modificări ale utilizării API, de exemplu, autentificare.

**Îmbunătățirea modularității tranzitului digital** <br>
Îmbunătățirea capacității de a încorpora serviciul pe diferite platforme de publicare.

**Dezvoltarea ulterioară a straturilor de hartă** <br>
Îmbunătățirea capacității de a aduce diferite surse de date pe straturile de hartă.

**Prezentarea informațiilor despre preț și conectarea la plată** <br>
Prezentarea informațiilor despre preț, zonele de plată, cu link-ul la cererea de plată

**Caracteristici de ciclism, v2.0** <br>
Dezvoltarea ulterioară a caracteristicilor de ciclism bazate pe feedback și nevoile curente pentru caracteristicile actuale.

**Integrarea serviciilor MaaS cu platforma OpenTransport și servicii bazate pe apel** <br>
Nu sunt posibile alternative în situații în care transportul public programat la destinație. Construirea suportului de date GTFS Flex.

**Contribuție la dezvoltarea OpenTripPlanner v2.0** <br>
Monitorizarea dezvoltarea versiunii de bază a OTP și participați la dezvoltare, după caz.

**Caracteristici API** <br>
Dezvoltarea funcțiilor API pentru a răspunde nevoilor terților și cerințelor privind traficul.

**Harta timpului de călătorie** <br>
Proiectarea și implementarea unui succesor la nivel național la serviciul actual de hartă a timpului de călătorie.

**Monitor virtual, v2.0** <br>
Dezvoltarea și adaptarea capabilităților monitorului virtual pentru cazuri noi de utilizare.

**Funcționalitatea de călătorie** <br>
OpenTransport oferă direcții dinamice în timpul călătoriei, sugerand alternative la rutele curente in cazul schimbarii directiei.

**Funcții de navigare** <br>
Fiecare pas al călătoriei are direcții pas cu pas, cum ar fi o listă de mers pe stradă și ghidare vocală.

**Reforma UI Comprehensive, OpenTransport v2.0** <br>
Îmbunătățirea accesibilității, a noilor soluții de utilizator și a dezvoltării generale a experienței utilizatorului.

**Conectare**<br>
Utilizarea informațiilor utilizatorului pentru a oferi un serviciu personalizat
