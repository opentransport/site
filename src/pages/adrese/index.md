﻿---
title: Căutare adresă
panels:
  - title: Căutare adresă
    body: OpenTransport este un pachet de servicii deținut și dezvoltat de OpenTransport pentru rutarea transportului public. Este posibil ca municipalitățile să se alăture serviciului.
    links: []
    image: "../users.svg"
    id: users
    textColor: "#000"
    swapped: true
---

## Sursa datelor

OpenTransport folosește următoarea sursă pentru datele ce privesc adresele:
- Conținut OpenStreetMap pe care utilizatorii își pot întreține și actualiza
- Baza de date de adrese a clădirilor produse de Centrul de evidență a populației
- Baza de date privind localizarea oferit de Cartea Funciara a României
- GTFS în ceea ce priveste datele de mobilitate în orașe

