---
title: Servicii
panels:
  - title: Produceți-vă propriile servicii Open Transport
    body: Puteți porni propriul set de servicii prin configurarea containerelor Docker și a bibliotecilor sursă furnizate de OpenTransport. Dacă doriți support sau o colaborare cu OpenTransport ne puteți contacta pe adresa **hello@opentransport.ro** sau folosind informațiile găsite in secțiunea **Colaborare**
    links: []
    image: "../cloud.svg"
    id: maintainers
    textColor: "#000"
    swapped: false
--- 

## Servicii Open Transport

Principalele componente ale OpenTransport sunt:

1. Motor de rutare
2. Căutare adresă
3. Hărți de fundal
4. Rutați interfața utilizatorului

Primele trei dintre serviciile menționate anterior necesită material sursă pentru a produce procese de încărcare a datelor corespunzătoare pe platforma de servicii.

Motorul de rutare OpenTripPlanner cu GTFS și date de hartă oferă o serie de caracteristici de planificare a călătoriilor, cum ar fi rutarea multimodală ușă la ușă, oprirea și căutarea liniei.

Motorul de căutare de adrese Pelias poate fi utilizat pentru a găsi coordonatele de poziție ale șirului introdus sau pentru a întreba ce locație sau adresă este cea mai apropiată de coordonata dată.

Serviciul de hărți de fundal oferă informații de hartă în rastrele și plăcile vectoriale.

Serviciul Interfață de utilizare Ghid de ruta oferă o interfață bazată pe browser care permite utilizarea altor servicii de pe platformă, de exemplu. efectuând căutări de rute.

Pe lângă acestea, OpenTransport include o varietate de servicii non-esențiale în timp real și accesorii pentru implementarea serviciului de rutare. O descriere mai detaliată a serviciilor parțiale poate fi găsită in zona [documentație pentru dezvoltatori](../en/developers/).


## Lansarea serviciilor si propria întreținere

Toate sub-serviciile OpenTransport sunt ambalate în containere Docker gata făcute, care pot fi rulate cu ușurință pe propria platformă de server.
Containerele pot fi, de asemenea, controlate într-o oarecare măsură prin variabile de mediu.

Cea mai ușoară soluție este utilizarea datelor sursă furnizate de OpenTransport (containerele de date OpenTripPlanner și Pelias și ot-map-server), eliminând necesitatea construirii unor procese de descărcare grea a datelor. Limitarea, desigur, este că conținutul datelor furnizate este același ca în OpenTransport.
Nu se poate ruta într-un oraș în care datele GTFS nu sunt cunoscute pe OpenTransport, iar căutarea adresei nu va găsi destinații în afara Romaniei.

Dacă datele dvs. sunt necesare pentru orice sub-serviciu, este necesar să copiați și să modificați procesul de descărcare a datelor OpenTransport după cum doriți.
În unele privințe, acest lucru poate fi realizat numai cu variabile de mediu (de exemplu, puteți adăuga noi surse de date la încărcarea datelor OpenTripPlanner),
dar, în general, este necesar să faceți propria versiune a bibliotecilor de coduri sursă gitlab / opentransport și să le modificați sau să vă construiți propriul serviciu de date.

Configurația de descărcare a datelor de traseu OpenTripPlanner este destul de ușor de personalizat pentru a utiliza propriile date. Configurația listează adresele web,
unde sunt preluate pachetele de date GTFS și adresa de recuperare a datelor de hartă OSM. Puteți modifica lista de adrese după dorință.

Materialul sursă de căutare a adreselor este definit de un set de scripturi care preia și prelucrează seturi de date.
Acestea sunt destul de ușor de adăugat, de eliminat și de modificat. Indexarea materialului individual la baza de date de adrese se face într-un mod personalizat
javascript biblioteca, de exemplu, https://gitlab.com/opentransport/pelias-gtfs. Sistemul de descărcare de date este încorporat adăugarea unei ierarhii administrative la proprietăți folosind regionalizarea romană solicitată de la WhosOnFirst. Această metodă este valabilă înlocuiți sau schimbați conținutul de date în afara Romaniei.

Descărcarea datelor de hartă de fundal este puternic legată de serviciile de hărți furnizate de OT. Este posibil să creați un serviciu de înlocuire a hărții în conformitate cu
[Ghid de documentare pentru dezvoltatori](../en/developers/apis/3-map-api).

De asemenea, este posibil să mențineți doar unele dintre servicii și să utilizați restul interfețelor OpenTransport; nou ghid local de călătorie în Romania poate profita cu ușurință de căutarea adreselor la nivel național.

Un exemplu de pornire a propriului serviciu de ghidare a rutei cu date de ghid de rute ot:

### 1. Instalați Docker pe server

### 2. Porniți motorul de rutare:

```bash
docker run -d --rm  -p 9080:8080 -e ROUTER_NAME=romania -e JAVA_OPTS=-Xmx4g -e ROUTER_DATA_CONTAINER_URL=https://api.opentransport.ro/routing-data/v1/romania opentransport:opentripplanner:prod
```

Rutarea este servită acum la localhost: 9080.

### 3. Porniți serviciul bazei de date a adreselor

```bash
docker run -d --rm --name pelias-data-container opentransport/pelias-data-container
```

### 4. Porniți serviciul de căutare a adreselor:

```bash
docker run -d --rm --name pelias-api -p 3100:8080 --link pelias-data-container:pelias-data-container opentransport/pelias-api
```

Căutarea adreselor este disponibilă la localhost: 3100. Deschide browserul și testează-ți căutarea: https://localhost:3100/v1/search?text=Timisoara

### 5. Porniți serviciul UI:

```bash
docker run -d --rm  -p 8080:8080 -e OTP_URL=http://localhost:9080/otp/routers/timisoara/ -e CONFIG=romania -e GEOCODING_BASE_URL=localhost:3100/v1 opentransport/ot-ui
```

Acum puteți naviga la localhost: 8080 și puteți începe să utilizați propriul serviciu de ghidare a rutelor private cu hărți de fundal provenind de la OpenTransport.

## Tema interfeței cu utilizatorul

Tema implicită din Itinerariu nu include funcții specifice orașului. Este configurat cu setări care se aplică tuturor datelor din Romania.

Interfața poate fi temată și configurată astfel încât să se potrivească zonei dorite prin crearea unui nou fișier de configurare în biblioteca de cod sursă https://gitlab.com/opentransport/ui dosarul `app/configurations`. Instrucțiuni detaliate pot fi găsite la adresa
https://gitlab.com/opentransport/ui/blob/master/docs/Themes.md .


## Resurse necesare

Exemple:

- Motorul de căutare pe ruta (OpenTripPlanner) cu date din zona timisoara complete necesită 4 GB RAM și un procesor puternic multi-core
- Căutarea adreselor la toate datele romanesti necesită 3 GB memorie centrală pentru serverul api (Pelias-api) și 4 GB memorie centrală pentru containerul de date Pelias. O unitate de procesare centrală eficientă grăbește serviciul.
- Server Map Background (map-server) necesită 4 GB RAM și un procesor de bază
- Serverul UI are nevoie de 1 GB memorie și un procesor de bază

Aceste resurse pot servi mai mulți utilizatori simultan aproape fără întârziere. Pe măsură ce sarcina crește, soluțiile de echilibrare a sarcinii trebuie implementate,
pentru a distribui interogări pe mai multe servere.

Este posibilă reducerea încărcării serviciilor și îmbunătățirea timpului de răspuns al serviciului cu diverse soluții de memorie în cache (CDN) proxy.
Mai ales serviciile de hartă de fundal beneficiază de acestea.


## Caracteristicile regionale ale serviciilor

Versiunea motorului de rutare OpenTripPlanner folosit de OpenTransport este destul de generică și nu conține modificări semnificative specifice Romaniei.
Unele caracteristici speciale, cum ar fi ticketingul local, pot fi activate prin configurare.

Căutarea adreselor Pelias a fost dezvoltată pentru a sprijini căutarea multilingvă. Specificul țării este specificat în fișierul de configurare.


## Link-uri către bibliotecile sursă

- [Motor de rutare](https://gitlab.com/opentransport/router)
- [Încărcarea a datelor de rutare](https://gitlab.com/opentransport/router-data-container)
- [Furnizor adresă căutare](https://gitlab.com/opentransport/pelias-api)
- [Încărcare datelor de căutare adresa](https://gitlab.com/opentransport/pelias-data-container)
- [Hărți](https://gitlab.com/opentransport/map-server)
- [Interfață web](https://gitlab.com/opentransport/ui)
