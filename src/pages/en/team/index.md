---
title: Meet the team
panels:
  - title: HI!🖐 MEET THE TEAM
    body: Curious to meet us and know what we do? Check out a brief description about us and our ideas.
    links: []
    image: "../../team.svg"
    id: team
    textColor: "#000"
    swapped: false
---

## WHO WE ARE, WHAT WE WANT

We’re a team of passionate 🤟 developers, and transit nerds who believe technology can make sustainable transport more popular than ever. Whether it's public transit or bikesharing, we believe the revolution taking over our streets will be captained from our phones. We invest in technology that brings your world close to our green fantasy.

We want to make the transportation easy to use for you. We accept challenges and transforming your needs into reality is our desire. So if your into it don't hesitate to thow us some words at [hello@opentransport.ro](mailto:hello@opentransport.ro).
  
<br/>
<br/>
<br/>

## THE NERDS 🤓 BEHIND
