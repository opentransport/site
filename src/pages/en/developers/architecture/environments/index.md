---
title: Environments
---

## Timisoara environments

Development:
> https://dev.opentransport.ro/

Production:
> https://opentransport.ro/

## Romania environments

<!-- Development:
> https://devtm.opentransport.ro/

Production:
> https:///tm.opentransport.ro/ -->

## 3rd party environments
OpenTransport does not host all services that are listed in service catalogue. Some of the services (e.g. realtime for 3rd party cities) are not hosted by OpenTransport. However, for your convenience we keep also up-to-date list for 3rd party services.  
