---
title: Real-time APIs
---
## About the API
Heavy lifting of real-time data is done in other systems. OpenTransport integrates itself to various datasources in order to read real-time data from the vehicles. How this is done depends on which region we are talking about.

![Modules](./architecture.png)

[Full size image (SVG)](./architecture.svg) (or click the image for full size PNG)

## Related open source projects

| URL                                                   | Project description                                          |
| ----------------------------------------------------- | ------------------------------------------------------------ |
| https://gitlab.com/opentransport/transitdata              | Processes HSL realtime data with Apache Pulsar. Produces GTFS-RT messages from internal data formats. 
| https://gitlab.com/opentransport/siri2gtfsrt              | Convert SIRI JSON to GTFS-RT trip updates                    |
| https://gitlab.com/opentransport/raildigitraffic2gtfsrt   | Converts digitraffic train updates into GTFS-RT Trip Updates |
| https://gitlab.com/opentransport/gtfsrthttp2mqtt          | Reads GTFS-RT over HTTP and publishes it as MQTT data        |
| https://developers.google.com/transit/                | Google transit community                                     |
| https://groups.google.com/forum/#!forum/gtfs-realtime | Google transit forum                                         |
