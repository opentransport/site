---
title: Geocoding API
description:
  info: Geocoder with geocoding and reverse geocoding support.
  architecture: https://gitlab.com/opentransport/site/raw/master/src/pages/en/developers/apis/2-geocoding-api/x-service-architecture/architecture.xml
assets:
  - title: "source"
    url: https://gitlab.com/opentransport/pelias-api
  - title: "DockerHub"
    url: https://hub.docker.com/r/opentransport/pelias-api/
  - title: "Dockerfile"
    url: https://gitlab.com/opentransport/pelias-api/blob/master/Dockerfile
  - title: "Pelias fuzzy tests"
    url: https://gitlab.com/opentransport/pelias-fuzzy-tests
  - title: "Pelias fuzzy tester"
    url: https://gitlab.com/opentransport/fuzzy-tester
docker:
  dockerfile: https://gitlab.com/opentransport/pelias-api/blob/master/Dockerfile
  imageName: opentransport/pelias-api
  buildScript: https://gitlab.com/opentransport/pelias-api/blob/master/build-docker-image.sh
  runContainer: docker run -d --name pelias-api -p 3100:8080 --link pelias-data-container opentransport/pelias-api
  accessContainer: curl "http://localhost:3100/v1/search?text=helsinki"
  travisBuild: pelias-api
---

## Running API tests

Read instructions from

> https://gitlab.com/opentransport/pelias-fuzzy-tests

## Related open source projects

| URL                                    | Project description                                 |
| -------------------------------------- | --------------------------------------------------- |
| https://github.com/pelias/pelias       | Pelias development on GitHub                        |
| https://mapzen.com/projects/search/    | Mapzen Search (which is essentially same as Pelias) |
| https://github.com/pelias/api          | Pelias-api upstream development on GitHub           |
| https://github.com/pelias/fuzzy-tester | Pelias fuzzy-tester upstream development on GitHub  |
