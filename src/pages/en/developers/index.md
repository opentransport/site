---
title: Developer Cookbook
---

OpenTransport Platform is an open source journey planning solution that combines several open source components into a
modern, highly available route planning service. Route planning algorithms and APIs are provided by Open Trip Planner
(OTP). OTP is a great solution for general route planning but in order to provide top-notch journey planning other
components such as Mobile friendly user interface, Map tile serving, Geocoding, and various data conversion tools are
needed. OpenTransport platform provides these tools.

## Licensing
The source code of the platform is dual-licensed under the EUPL v1.2 and AGPLv3 licenses.

## Issues
Our issue tracking is handled in [https://gitlab.com/groups/opentransport/-/issues](https://gitlab.com/groups/opentransport/-/issues).

## Key features

* Multimodal routing
* Realtime information support
* Mobile friendly user interface that supports themes
* Configurable for any region

## In production

* [Romania - Timisoara region](http://tm.opentransport.ro/)
