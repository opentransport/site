---
title: Itinerary planning
order: 80
---

**If you are not yet familiar with [GraphQL](../0-graphql) and [GraphiQL](../1-graphiql) it is highly recommended to review those pages at first.**

## Glossary

| Term                   | Explanation                     |
|------------------------|---------------------------------|
| Itinerary              | A combination of different transportation modes at certain times to reach from origin to destination. For example, walking to a bus stop, taking a bus for two stops and then walking to the final destination.<br/>Commonly used synonym: journey |
| Leg                    | One part of an itinerary, e.g. walking to a bus stop or a bus ride between two stops. |
| Origin                 | A geographical point where an itinerary begins. |
| Destination            | A geographical point where an itinerary ends. |

### Note about Itinerary leg geometries

You can ask the server to return geometries for itineraries. The API will return them in [Google polyline-encoded format](https://developers.google.com/maps/documentation/utilities/polylinealgorithm). It looks like this:
```json
"legGeometry": {
  "length": 349,
  "points": "wwfnJyjdwCXlAHLNfAFHDZz@nEh@hBXfA\\hAR^VLnChAD@HDHDjBt@^N|@`@JDHNFB`@RHDJFD@PD^fBDNNp@@JBPHl@FdA@d@@TFrCDnATpEL~CBnA@pADpELbQ?pB?nBC~@DdC?R?LDtC?P?j@@XG~@Ef@CPCHGHoAtAQT_@f@w@rAc@~@g@lAi@fBc@hBWvASzAWrBUtBQxBOtBMhCOvEEhCCfC@vB@fBDrCFlCJnCb@xIfFl_APdDr@dMd@bIV~D\\bFPnCjAtMt@~GxAhMlBfOz@jHVjCR~BNpCHrBFjCDdC?`CC`DIfDSxDUhDe@fFu@tHMzA{AlOcBtPCXE`@sCrY_BbPShCeAxJc@pDg@nECJi@~Di@dDUbAkA~Gk@bD_@vBUbB]lBe@lDY~B[rCWbC_@~DYfDW|BQ~Am@tGOfBOxAWlCKzAMvAIbAMpBIpAAd@GhACp@EhAEtAA~ACfCAnC@fA?rAClDC`G@`BCxHKlCMhIK~FE|CA~@@|@BxB@^?@DtAHbBLzALjBHbBFlAF|ADxAD|BNrEPjCTtHdAhVp@jSVlPFjJ?dC?hE?dJ?xC?vLDfKDpEF|CFnDPjHZvH\\xH|@jQXnF\\nFBXdAtSJtCZrG@^b@`K^dJLnDLtDj@bRT`IAb@?~@?dA@~A?fA?|@AjAE~BA`BBdABd@Bh@HfARnBPpBl@zFt@lGr@vE\\|Cz@dHrA|JdAtHPpAl@dEjA~I~AvNv@tIx@vKTtD`@nHTrENtDRzEPvHR|J\\`VNdKAjEFpLBlFDjCBfCFxBNjE`@nH@j@An@Cr@I^O^y@l@iAt@S@IAIGMKCICIKIKEI?MDILKVQVW^WVUPSJuAT??_ANaALg@No@ZkAz@a@ZKR]j@a@h@i@fA[f@Q`@u@dCmAvEw@dCk@nAe@z@S\\iAnAeA`Ai@f@eAfBy@rBu@rCMf@WfAU`Ac@`Bi@`CKh@ANUdBq@~GW|BQdAK^M\\IROVMLs@d@q@\\]P}@R{@BaA@_@Iu@E]?CAU?M?W@i@DKBg@NUDq@V}@^g@XQJk@h@EDQPeBnBIHILU\\"
}
```

You can use [this tool](https://repl.it/@opentransport/Polyline-Mapper) to see what the polyline looks on a map.

See the following examples on how to decode and use polylines:
* [Node.js app](https://repl.it/@opentransport/PolylineDecode), which prints a list of coordinates in an itinerary
* [Itinerary planner demo](https://repl.it/@opentransport/PolylineDecodeMap), which shows itinerary leg polylines on a map

## Query examples

**Note:** For more details about the query type **plan** and its parameters you can use the **Documentation Explorer** provided in GraphiQL.

Itinerary planning can be tuned by multiple arguments of the **plan** query.
* Time arguments (e.g. `minTransferTime`, `bikeSwitchTime`) are taken into account literally when planning the itinerary
  * For example, if `minTransferTime` is set to 2 minutes, it is not possible to continue the journey by another vehicle within two minutes after disembarking one vehicle
  * Values of time arguments are included in the returned duration of an itinerary
    * For example, if there is a 15 minute bicycling leg and `bikeSwitchTime` is set to 1 minute, the returned duration of the bicycling leg will be 17 minutes
* Cost arguments (e.g. `walkBoardCost`) on the other hand are not hard limits, but preferences
  * For example, if `walkBoardCost` is set to 2 minutes, it is possible to continue the journey immediately after disembarking from one vehicle, but up to 2 minutes longer itineraries are preferred if they have one transfer less and up to 4 minutes longer itineraries are preferred if they have two transfers less, etc.
  * Cost is not included in the returned duration of an itinerary
    * For example, if there is a 15 minute bicycling leg and `bikeSwitchCost` is set to 1 minute, the returned duration of the bicycling leg will be 15 minutes
* Multiplier arguments (e.g. `walkReluctance`, `modeWeight`) are used to multiply costs of an leg
  * For example, if `walkReluctance` is set to 3.0, the cost of each walking section will be multiplied by 3 and thus itineraries with less walking are preferred

### Plan an itinerary from location (60.168992,24.932366) to (60.175294,24.684855)

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520plan%28%250A%2520%2520%2520%2520from%253A%2520%257Blat%253A%252045.744620%252C%2520lon%253A%252021.246866%257D%250A%2520%2520%2520%2520to%253A%2520%257Blat%253A%252045.751619%252C%2520lon%253A%252021.216655%257D%250A%2520%2520%2520%2520numItineraries%253A%25203%250A%2520%2520%29%2520%257B%250A%2520%2520%2520%2520itineraries%2520%257B%250A%2520%2520%2520%2520%2520%2520legs%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520startTime%250A%2520%2520%2520%2520%2520%2520%2520%2520endTime%250A%2520%2520%2520%2520%2520%2520%2520%2520mode%250A%2520%2520%2520%2520%2520%2520%2520%2520duration%250A%2520%2520%2520%2520%2520%2520%2520%2520realTime%250A%2520%2520%2520%2520%2520%2520%2520%2520distance%250A%2520%2520%2520%2520%2520%2520%2520%2520transitLeg%250A%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D) to run the query below in GraphiQL.

```graphql
{
  plan(
    from: {lat: 45.744620, lon: 21.246866}
    to: {lat: 45.751619, lon: 21.216655}
    numItineraries: 3
  ) {
    itineraries {
      legs {
        startTime
        endTime
        mode
        duration
        realTime
        distance
        transitLeg
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Basic route from Complexul Studentesc (Complex) to Iulius Town (Lipovei)

* Origin and destination locations can be named by using arguments `fromPlace` and `toPlace` instead of `to` and `from`
  * Values for arguments `fromPlace` and `toPlace` are in format `<name>::<lat>,<lng>`


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520plan%28%250A%2520%2520%2520%2520fromPlace%253A%2520%2522Complex%2520Studentesc%253A%253A45.748078%252C21.236596%2522%252C%250A%2520%2520%2520%2520toPlace%253A%2520%2522Iulius%2520Town%253A%253A45.766413%252C%252021.227519%2522%252C%250A%2520%2520%29%2520%257B%250A%2520%2520%2520%2520itineraries%257B%250A%2520%2520%2520%2520%2520%2520walkDistance%252C%250A%2520%2520%2520%2520%2520%2520duration%252C%250A%2520%2520%2520%2520%2520%2520legs%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520mode%250A%2520%2520%2520%2520%2520%2520%2520%2520startTime%250A%2520%2520%2520%2520%2520%2520%2520%2520endTime%250A%2520%2520%2520%2520%2520%2520%2520%2520from%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stop%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520code%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%252C%250A%2520%2520%2520%2520%2520%2520%2520%2520to%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%252C%250A%2520%2520%2520%2520%2520%2520%2520%2520agency%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520gtfsId%250A%2509%2520%2520%2509%2509%2509name%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%252C%250A%2520%2520%2520%2520%2520%2520%2520%2520distance%250A%2520%2520%2520%2520%2520%2520%2520%2520legGeometry%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520length%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520points%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D) to run the query below in GraphiQL.

```graphql
{
  plan(
    fromPlace: "Complex Studentesc::45.748078,21.236596",
    toPlace: "Iulius Town::45.766413,21.227519",
  ) {
    itineraries{
      walkDistance,
      duration,
      legs {
        mode
        startTime
        endTime
        from {
          lat
          lon
          name
          stop {
            code
            name
          }
        },
        to {
          lat
          lon
          name
        },
        agency {
          gtfsId
	  name
        },
        distance
        legGeometry {
          length
          points
        }
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Plan an itinerary using only WALK and TRAM modes

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%250A%257B%250A%2520%2520plan%28%250A%2520%2520%2520%2520from%253A%2520%257Blat%253A%252045.744620%252C%2520lon%253A%252021.246866%257D%250A%2520%2520%2520%2520to%253A%2520%257Blat%253A%252045.751619%252C%2520lon%253A%252021.216655%257D%250A%2520%2520%2520%2520numItineraries%253A%25203%250A%2520%2520%2509transportModes%253A%2520%255B%257Bmode%253A%2520WALK%257D%252C%2520%257Bmode%253A%2520TRAM%257D%255D%250A%2520%2520%29%2520%257B%250A%2520%2520%2520%2520itineraries%2520%257B%250A%2520%2520%2520%2520%2520%2520legs%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520startTime%250A%2520%2520%2520%2520%2520%2520%2520%2520endTime%250A%2520%2520%2520%2520%2520%2520%2520%2520mode%250A%2520%2520%2520%2520%2520%2520%2520%2520duration%250A%2520%2520%2520%2520%2520%2520%2520%2520realTime%250A%2520%2520%2520%2520%2520%2520%2520%2520distance%250A%2520%2520%2520%2520%2520%2520%2520%2520transitLeg%250A%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D) to run the query below in GraphiQL.

```graphql
{
  plan(
    from: {lat: 45.744620, lon: 21.246866}
    to: {lat: 45.751619, lon: 21.216655}
    numItineraries: 3
    transportModes: [{mode: WALK}, {mode: TRAM}]
  ) {
    itineraries {
      legs {
        startTime
        endTime
        mode
        duration
        realTime
        distance
        transitLeg
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Plan an itinerary from Hakaniemi to Keilaniemi and modify the following parameters:

* Return five results: (`numItineraries: 5`)
* Use transportation modes other than subway (`transportModes`)
* Walking speed of 1,7m/s (`walkSpeed: 1.7`)
* Use a 10 minute safety margin for transfers (`minTransferTime: 600`)
* Use a 5 minute boarding cost (`walkBoardCost: 300`)
  * Boarding cost is used to prefer itineraries with less vehicle boardings
    * For example, if `walkBoardCost: 300` is used and there is a 48min itinerary with one boarding and a 45min itinerary with two boardings, the 48 minute itinerary is returned, because its total cost is smaller (48min + 5min vs. 45min + 5min + 5min)
* Use multiplier of 2.1 for walk reluctance to prefer routes with less walking (`walkReluctance: 2.1`)
  * Walking times are multiplied with this multiplier
* Specific departure date and time
  * `date` in format YYYY-MM-DD
  * `time` in format hh:mm:ss


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520plan%28%250A%2520%2520%2520%2520fromPlace%253A%2520%2522Shopping%2520City%253A%253A45.725141%252C21.200891%2522%252C%250A%2520%2520%2520%2520toPlace%253A%2520%2522Dumbravita%253A%253A45.786172%252C21.237509%2522%252C%250A%2520%2520%2520%2520date%253A%2520%25222019-12-21%2522%252C%250A%2520%2520%2520%2520time%253A%2520%252223%253A28%253A00%2522%252C%250A%2520%2520%2520%2520numItineraries%253A%25205%252C%250A%2520%2520%2520%2520transportModes%253A%2520%255B%257Bmode%253A%2520BUS%257D%252C%2520%257Bmode%253A%2520FUNICULAR%257D%252C%2520%257Bmode%253ATRAM%257D%252C%2520%257Bmode%253AWALK%257D%255D%250A%2520%2520%2520%2520walkReluctance%253A%25202.1%252C%250A%2520%2520%2520%2520walkBoardCost%253A%2520300%252C%250A%2520%2520%2520%2520minTransferTime%253A%2520600%252C%250A%2520%2520%2520%2520walkSpeed%253A%25201.7%252C%250A%2520%2520%29%2520%257B%250A%2520%2520%2520%2520itineraries%257B%250A%2520%2520%2520%2520%2520%2520walkDistance%250A%2520%2520%2520%2520%2520%2520duration%250A%2520%2520%2520%2520%2520%2520legs%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520mode%250A%2520%2520%2520%2520%2520%2520%2520%2520startTime%250A%2520%2520%2520%2520%2520%2520%2520%2520endTime%250A%2520%2520%2520%2520%2520%2520%2520%2520from%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stop%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520code%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520to%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stop%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520code%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520trip%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2509tripHeadsign%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520routeShortName%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520distance%250A%2520%2520%2520%2520%2520%2520%2520%2520legGeometry%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520length%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520points%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D) to run the query below in GraphiQL.

```graphql
{
  plan(
    fromPlace: "Shopping City::45.725141,21.200891",
    toPlace: "Dumbravita::45.786172,21.237509",
    date: "2019-12-21",
    time: "23:28:00",
    numItineraries: 5,
    transportModes: [{mode: BUS}, {mode: FUNICULAR}, {mode:TRAM}, {mode:WALK}]
    walkReluctance: 2.1,
    walkBoardCost: 300,
    minTransferTime: 600,
    walkSpeed: 1.7,
  ) {
    itineraries{
      walkDistance
      duration
      legs {
        mode
        startTime
        endTime
        from {
          lat
          lon
          name
          stop {
            code
            name
          }
        }
        to {
          lat
          lon
          name
          stop {
            code
            name
          }
        }
        trip {
        	tripHeadsign
          routeShortName
        }
        distance
        legGeometry {
          length
          points
        }
      }
    }
  }
}
```

2. Change arguments `date` and `time`.
3. Press play in GraphiQL to execute the query.

### Plan an itinerary using Park & Ride

* Using qualifier **PARK** for **CAR** mode plans an itinerary using Park & Ride, i.e. the first leg of the journey is done by driving to a car park and continuing by public transportation from there


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520plan%28%250A%2520%2520%2520%2520fromPlace%253A%2520%2522Shopping%2520City%253A%253A45.725141%252C21.200891%2522%252C%250A%2520%2520%2520%2520toPlace%253A%2520%2522Dumbravita%253A%253A45.786172%252C21.237509%2522%252C%250A%2520%2520%2520%2520transportModes%253A%2520%255B%257Bmode%253A%2520CAR%252C%2520qualifier%253A%2520PARK%257D%252C%2520%257Bmode%253A%2520TRANSIT%257D%252C%2520%257Bmode%253AWALK%257D%255D%250A%2520%2520%29%2520%257B%250A%2520%2520%2520%2520itineraries%257B%250A%2520%2520%2520%2520%2520%2520walkDistance%250A%2520%2520%2520%2520%2520%2520duration%250A%2520%2520%2520%2520%2520%2520legs%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520mode%250A%2520%2520%2520%2520%2520%2520%2520%2520startTime%250A%2520%2520%2520%2520%2520%2520%2520%2520endTime%250A%2520%2520%2520%2520%2520%2520%2520%2520duration%250A%2520%2520%2520%2520%2520%2520%2520%2520from%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stop%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520code%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520to%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stop%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520code%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520carPark%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520carParkId%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520trip%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2509tripHeadsign%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520routeShortName%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520distance%250A%2520%2520%2520%2520%2520%2520%2520%2520legGeometry%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520length%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520points%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D) to run the query below in GraphiQL.

```graphql
{
  plan(
    fromPlace: "Shopping City::45.725141,21.200891",
    toPlace: "Dumbravita::45.786172,21.237509",
    transportModes: [{mode: CAR, qualifier: PARK}, {mode: TRANSIT}, {mode:WALK}]
  ) {
    itineraries{
      walkDistance
      duration
      legs {
        mode
        startTime
        endTime
        duration
        from {
          lat
          lon
          name
          stop {
            code
            name
          }
        }
        to {
          lat
          lon
          name
          stop {
            code
            name
          }
          carPark {
            carParkId
            name
          }
        }
        trip {
        	tripHeadsign
          routeShortName
        }
        distance
        legGeometry {
          length
          points
        }
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Plan an itinerary and query fare information

* **Note:** Currently no fare information is available


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20plan(%0A%20%20%20%20from%3A%20%7Blat%3A%2060.1713572%2C%20lon%3A%2024.9416544%7D%0A%20%20%20%20to%3A%20%7Blat%3A%2060.40431%2C%20lon%3A%2025.1066186%7D%0A%20%20%20%20numItineraries%3A%203%0A%20%20)%20%7B%0A%20%20%20%20date%0A%20%20%20%20itineraries%20%7B%0A%20%20%20%20%20%20legs%20%7B%0A%20%20%20%20%20%20%20%20startTime%0A%20%20%20%20%20%20%20%20endTime%0A%20%20%20%20%20%20%20%20mode%0A%20%20%20%20%20%20%20%20duration%0A%20%20%20%20%20%20%20%20realTime%0A%20%20%20%20%20%20%20%20distance%0A%20%20%20%20%20%20%20%20transitLeg%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20fares%20%7B%0A%20%20%20%20%20%20%20%20type%0A%20%20%20%20%20%20%20%20cents%0A%20%20%20%20%20%20%20%20currency%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D) to run the query below in GraphiQL.

```graphql
{
  plan(
    from: {lat: 45.744620, lon: 21.246866}
    to: {lat: 45.751619, lon: 21.216655}
    numItineraries: 3
  ) {
    date
    itineraries {
      legs {
        startTime
        endTime
        mode
        duration
        realTime
        distance
        transitLeg
      }
      fares {
        type
        cents
        currency
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Plan an itinerary with ticket type restrictions

#### Query list of available ticket types

* Field `fareId` contains ticket type ID that can be used with **plan** query
* Field `zones` contains a list of zones where the ticket is valid


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20ticketTypes%20%7B%0A%20%20%20%20fareId%0A%20%20%20%20price%0A%20%20%20%20currency%0A%20%20%20%20zones%0A%20%20%7D%0A%7D) to run the query below in GraphiQL.

```graphql
{
  ticketTypes {
    fareId
    price
    currency
    zones
  }
}
```

2. Press play in GraphiQL to execute the query.

#### Plan an itinerary with AB ticket

* The following query plans an itinerary from Helsinki (zone A) to Tikkurila (zone C) using only AB ticket


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20plan(%0A%20%20%20%20from%3A%20%7Blat%3A%2060.1713572%2C%20lon%3A%2024.9416544%7D%0A%20%20%20%20to%3A%20%7Blat%3A%2060.29280%2C%20lon%3A%2025.04396%7D%0A%20%20%20%20numItineraries%3A%203%0A%20%20%20%20allowedTicketTypes%3A%20%22HSL%3AAB%22%0A%20%20)%20%7B%0A%20%20%20%20date%0A%20%20%20%20itineraries%20%7B%0A%20%20%20%20%20%20legs%20%7B%0A%20%20%20%20%20%20%20%20startTime%0A%20%20%20%20%20%20%20%20endTime%0A%20%20%20%20%20%20%20%20mode%0A%20%20%20%20%20%20%20%20route%20%7B%0A%20%20%20%20%20%20%20%20%20%20gtfsId%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20from%20%7B%0A%20%20%20%20%20%20%20%20%20%20name%0A%20%20%20%20%20%20%20%20%20%20stop%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20zoneId%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20to%20%7B%0A%20%20%20%20%20%20%20%20%20%20name%0A%20%20%20%20%20%20%20%20%20%20stop%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20zoneId%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20duration%0A%20%20%20%20%20%20%20%20distance%0A%20%20%20%20%20%20%20%20transitLeg%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D) to run the query below in GraphiQL.

```graphql
{
  plan(
    from: {lat: 60.1713572, lon: 24.9416544}
    to: {lat: 60.29280, lon: 25.04396}
    numItineraries: 3
    allowedTicketTypes: "HSL:AB"
  ) {
    date
    itineraries {
      legs {
        startTime
        endTime
        mode
        route {
          gtfsId
        }
        from {
          name
          stop {
            zoneId
          }
        }
        to {
          name
          stop {
            zoneId
          }
        }
        duration
        distance
        transitLeg
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.