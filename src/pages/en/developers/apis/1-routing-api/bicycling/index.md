---
title: Bicycling
order: 50
---

**If you are not yet familiar with [GraphQL](../0-graphql) and [GraphiQL](../1-graphiql) it is highly recommended to review those pages at first.**

## Bicycle related query types

The Routing API provides a few bicycle related query types:

- Query type **plan** can be used to query bicycling routes using either a city bike or your personal bike
- Query types **bikeRentalStation** and **bikeRentalStations** can be used to query city bike rental stations and bikes that are available
- Query types **bikePark** and **bikeParks** can be used to query bike parks that are available

**Note:** For more details about these query types you can use the **Documentation Explorer** provided in GraphiQL.

## City bikes

**Note:** City bike API data is realtime and it is always up to date.

> https://www.velotm.ro

## Query examples

**Note:** If the examples provided with an ID do not return what is expected then the ID in question may not be in use any more and you should try again with an existing ID.

### All available city bike stations

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20bikeRentalStations%20%7B%0A%20%20%20%20name%0A%20%20%20%20stationId%0A%20%20%7D%0A%7D) to run the query below in GraphiQL. It should fetch all available city bike stations.

```graphql
{
  bikeRentalStations {
    name
    stationId
  }
}
```

2. Press play in GraphiQL to execute the query.

### Single city bike station and its current bike availability details

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20bikeRentalStation(id%3A%2222%22)%20%7B%0A%20%20%20%20stationId%0A%20%20%20%20name%0A%20%20%20%20bikesAvailable%0A%20%20%20%20spacesAvailable%0A%20%20%20%20lat%0A%20%20%20%20lon%0A%20%20%20%20allowDropoff%0A%20%20%7D%0A%7D) to run the query below in GraphiQL. It should fetch the city bike station and its current bike availability details.

```graphql
{
  bikeRentalStation(id:"22") {
    stationId
    name
    bikesAvailable
    spacesAvailable
    lat
    lon
    allowDropoff
  }
}
```

2. Press play in GraphiQL to execute the query.

### All available bike parks

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%0A%7B%0A%20%20bikeParks%20%7B%0A%20%20%20%20bikeParkId%0A%20%20%20%20name%0A%20%20%7D%0A%7D) to run the query below in GraphiQL. It should fetch all available bike parks.

```graphql
{
  bikeParks {
    bikeParkId
    name
  }
}
```

2. Press play in GraphiQL to execute the query.

### Single bike park

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%0A%7B%0A%20%20bikePark(id%3A%20%22906%22)%20%7B%0A%20%20%20%20bikeParkId%0A%20%20%20%20name%0A%20%20%20%20spacesAvailable%0A%20%20%20%20lat%0A%20%20%20%20lon%0A%20%20%7D%0A%7D) to run the query below in GraphiQL. It should fetch the bike park and its current space availability details.

```graphql
{
  bikePark(id: "906") {
    bikeParkId
    name
    spacesAvailable
    lat
    lon
  }
}
```

2. Press play in GraphiQL to execute the query.

### Plan an itinerary from Complexul Studentesc to Iulius Town using city bike rental

* Bike rental can be used by adding qualifier **RENT** to `transportModes` argument
  * Note that field `mode` in the results does not include information about qualifiers

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520plan%28%250A%2520%2520%2520%2520fromPlace%253A%2520%2522Complex%2520Studentesc%253A%253A45.748078%252C21.236596%2522%252C%250A%2520%2520%2520%2520toPlace%253A%2520%2522Iulius%2520Town%253A%253A45.766413%252C%252021.227519%2522%252C%250A%2520%2520%2520%2520numItineraries%253A%25201%252C%250A%2520%2520%2520%2520transportModes%253A%2520%255B%257Bmode%253A%2520BICYCLE%252C%2520qualifier%253A%2520RENT%257D%252C%2520%257Bmode%253A%2520WALK%257D%255D%252C%250A%2520%2520%29%2520%257B%250A%2520%2520%2520%2520itineraries%257B%250A%2520%2520%2520%2520%2520%2520walkDistance%250A%2520%2520%2520%2520%2520%2520duration%250A%2520%2520%2520%2520%2520%2520legs%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520mode%250A%2520%2520%2520%2520%2520%2520%2520%2520startTime%250A%2520%2520%2520%2520%2520%2520%2520%2520endTime%250A%2520%2520%2520%2520%2520%2520%2520%2520from%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520bikeRentalStation%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stationId%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520to%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520bikeRentalStation%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stationId%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520distance%250A%2520%2520%2520%2520%2520%2520%2520%2520legGeometry%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520length%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520points%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D) to run the query below in GraphiQL. It should plan an itinerary using city bike rental and show which rental stations are used.

```graphql
{
  plan(
    fromPlace: "Complex Studentesc::45.748078,21.236596",
    toPlace: "Iulius Town::45.766413, 21.227519",
    numItineraries: 1,
    transportModes: [{mode: BICYCLE, qualifier: RENT}, {mode: WALK}],
  ) {
    itineraries{
      walkDistance
      duration
      legs {
        mode
        startTime
        endTime
        from {
          lat
          lon
          name
          bikeRentalStation {
            stationId
            name
          }
        }
        to {
          lat
          lon
          name
          bikeRentalStation {
            stationId
            name
          }
        }
        distance
        legGeometry {
          length
          points
        }
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Plan an itinerary from Shoping City to Dumbravita riding your personal bike

* Note that transport mode **WALK** must not be used when planning an itinerary with personal bike, as otherwise the whole journey is done by walking

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520plan%28%250A%2520%2520%2520%2520fromPlace%253A%2520%2522Shopping%2520City%253A%253A45.725141%252C21.200891%2522%252C%250A%2520%2520%2520%2520toPlace%253A%2520%2522Dumbravita%253A%253A45.786172%252C21.237509%2522%252C%250A%2520%2520%2520%2520transportModes%253A%2520%257Bmode%253A%2520BICYCLE%257D%250A%2520%2520%29%2520%257B%250A%2520%2520%2520%2520itineraries%257B%250A%2520%2520%2520%2520%2520%2520walkDistance%250A%2520%2520%2520%2520%2520%2520duration%250A%2520%2520%2520%2520%2520%2520legs%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520mode%250A%2520%2520%2520%2520%2520%2520%2520%2520startTime%250A%2520%2520%2520%2520%2520%2520%2520%2520endTime%250A%2520%2520%2520%2520%2520%2520%2520%2520from%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520to%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520distance%250A%2520%2520%2520%2520%2520%2520%2520%2520legGeometry%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520length%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520points%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D%250A) to run the query below in GraphiQL. It should fetch bicycle route from Kamppi to Pisa.

```graphql
{
  plan(
    fromPlace: "Shopping City::45.725141,21.200891",
    toPlace: "Dumbravita::45.786172,21.237509",
    transportModes: {mode: BICYCLE}
  ) {
    itineraries{
      walkDistance
      duration
      legs {
        mode
        startTime
        endTime
        from {
          lat
          lon
          name
        }
        to {
          lat
          lon
          name
        }
        distance
        legGeometry {
          length
          points
        }
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Plan an itinerary from Giroc to Ghiroda and use your personal bike for the first part of the journey

* Using qualifier **PARK** for **BICYCLE** mode plans an itinerary, which begins by bicycling to a bike park from which the journey is continued by public transportation

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520plan%28%250A%2520%2520%2520%2520fromPlace%253A%2520%2522Giroc%253A%253A45.699270%252C21.235142%2522%252C%250A%2520%2520%2520%2520toPlace%253A%2520%2522Ghiroda%253A%253A45.764336%252C21.295344%2522%252C%250A%2520%2520%2520%2520numItineraries%253A%25201%252C%250A%2520%2520%2520%2520transportModes%253A%2520%255B%257Bmode%253A%2520BICYCLE%252C%2520qualifier%253A%2520PARK%257D%252C%2520%257Bmode%253A%2520WALK%257D%252C%2520%257Bmode%253A%2520TRANSIT%257D%255D%252C%250A%2520%2520%29%2520%257B%250A%2520%2520%2520%2520itineraries%257B%250A%2520%2520%2520%2520%2520%2520walkDistance%250A%2520%2520%2520%2520%2520%2520duration%250A%2520%2520%2520%2520%2520%2520legs%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520mode%250A%2520%2520%2520%2520%2520%2520%2520%2520startTime%250A%2520%2520%2520%2520%2520%2520%2520%2520endTime%250A%2520%2520%2520%2520%2520%2520%2520%2520from%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stop%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520gtfsId%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520code%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520to%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520bikePark%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520bikeParkId%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stop%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520gtfsId%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520code%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520trip%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520routeShortName%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520tripHeadsign%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520distance%250A%2520%2520%2520%2520%2520%2520%2520%2520legGeometry%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520length%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520points%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D%250A)  to run the query below in GraphiQL.

```graphql
{
  plan(
    fromPlace: "Giroc::45.699270,21.235142",
    toPlace: "Ghiroda::45.764336,21.295344",
    numItineraries: 1,
    transportModes: [{mode: BICYCLE, qualifier: PARK}, {mode: WALK}, {mode: TRANSIT}],
  ) {
    itineraries{
      walkDistance
      duration
      legs {
        mode
        startTime
        endTime
        from {
          lat
          lon
          name
          stop {
            gtfsId
            code
            name
          }
        }
        to {
          lat
          lon
          name
          bikePark {
            bikeParkId
            name
          }
          stop {
            gtfsId
            code
            name
          }
        }
        trip {
          routeShortName
          tripHeadsign
        }
        distance
        legGeometry {
          length
          points
        }
      }
    }
  }
}

```

2. Press play in GraphiQL to execute the query.

### Plan an itinerary from Stadion to Pasajul Jiul using personal bike and optimizing for safety

* Argument `optimize: SAFE` can be used to set preference for safer routes, i.e. avoid crossing streets and use bike paths when possible

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%250A%257B%250A%2520%2520plan%28%250A%2520%2520%2520%2520fromPlace%253A%2520%252245.744620%252C21.246866%2522%250A%2520%2520%2520%2520toPlace%253A%2520%252245.751619%252C21.216655%2522%250A%2520%2520%2509transportModes%253A%2520%255B%257Bmode%253A%2520BICYCLE%257D%255D%250A%2520%2520%2520%2520optimize%253A%2520SAFE%250A%2520%2520%29%2520%257B%250A%2520%2520%2520%2520itineraries%2520%257B%250A%2520%2520%2520%2520%2520%2520legs%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520mode%250A%2520%2520%2520%2520%2520%2520%2520%2520duration%250A%2520%2520%2520%2520%2520%2520%2520%2520distance%250A%2520%2520%2520%2520%2520%2520%2520%2520legGeometry%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520points%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D) to run the query below in GraphiQL.

```graphql
{
  plan(
    fromPlace: "45.744620,21.246866"
    toPlace: "45.751619,21.216655"
    transportModes: [{mode: BICYCLE}]
    optimize: SAFE
  ) {
    itineraries {
      legs {
        mode
        duration
        distance
        legGeometry {
          points
        }
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

