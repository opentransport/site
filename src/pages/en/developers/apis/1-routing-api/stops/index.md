---
title: Stops
order: 100
---

**If you are not yet familiar with [GraphQL](../0-graphql) and [GraphiQL](../1-graphiql) it is highly recommended to review those pages at first.**

## Notes about stop IDs

- Stop IDs are in `FeedId:StopId` format
- Timisoara area feed ID is **stpt**
- Stop ID is available from field `gtfsId` (note that field `gtfsId` also contains the feed ID)

## Query examples

**Note:** For more details about the query types related to stops you can use the **Documentation Explorer** provided in GraphiQL.

**Note:** If the examples provided with an ID do not return what is expected then the ID in question may not be in use any more and you should try again with an existing ID.

### Query all stops, returning their ID, name, location and zone

* Value of `zoneId` tells which zone the stop is located in

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20stops%20%7B%0A%20%20%20%20gtfsId%0A%20%20%20%20name%0A%20%20%20%20lat%0A%20%20%20%20lon%0A%20%20%20%20zoneId%0A%20%20%7D%0A%7D) to run the query below in GraphiQL.

```graphql
{
  stops {
    gtfsId
    name
    lat
    lon
    zoneId
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query stop by ID

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20stop(id%3A%20%22stpt%3A2987%22)%20%7B%0A%20%20%20%20name%0A%20%20%20%20wheelchairBoarding%0A%20%20%7D%0A%7D) to run the query below in GraphiQL.

```graphql
{
  stop(id: "stpt:2987") {
    name
    wheelchairBoarding
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query stop by ID and information about routes that go through it

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20stop(id%3A%20%22stpt%3A2987%22)%20%7B%0A%20%20%20%20gtfsId%0A%20%20%20%20name%0A%20%20%20%20lat%0A%20%20%20%20lon%0A%20%20%20%20patterns%20%7B%0A%20%20%20%20%20%20code%0A%20%20%20%20%20%20directionId%0A%20%20%20%20%20%20headsign%0A%20%20%20%20%20%20route%20%7B%0A%20%20%20%20%20%20%20%20gtfsId%0A%20%20%20%20%20%20%20%20shortName%0A%20%20%20%20%20%20%20%20longName%0A%20%20%20%20%20%20%20%20mode%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D) to run the query below in GraphiQL.

```graphql
{
  stop(id: "stpt:2987") {
    gtfsId
    name
    lat
    lon
    patterns {
      code
      directionId
      headsign
      route {
        gtfsId
        shortName
        longName
        mode
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query stops by name or number

* Argument `name` can either be a part of the stop name (e.g. `"complex"` or `"iulius"`) or a stop number (e.g. `"4040"`)


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520stops%28name%253A%2520%2522complex%2522%29%2520%257B%250A%2520%2520%2520%2520gtfsId%250A%2520%2520%2520%2520name%250A%2520%2520%2520%2520code%250A%2520%2520%2520%2520lat%250A%2520%2520%2520%2520lon%250A%2520%2520%257D%250A%257D) to run the query below in GraphiQL.

```graphql
{
  stops(name: "complex") {
    gtfsId
    name
    code
    lat
    lon
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query all stations and return list of stops within the stations

* Station is a location, which contains stops
* For example, a train station is a station and its platforms are stops


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%20%0A%20%20stations%20%7B%0A%20%20%20%20gtfsId%0A%20%20%20%20name%0A%20%20%20%20lat%0A%20%20%20%20lon%0A%20%20%20%20stops%20%7B%0A%20%20%20%20%20%20gtfsId%0A%20%20%20%20%20%20name%0A%20%20%20%20%20%20code%0A%20%20%20%20%20%20platformCode%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D) to run the query below in GraphiQL.

```graphql
{
  stations {
    gtfsId
    name
    lat
    lon
    stops {
      gtfsId
      name
      code
      platformCode
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query stations by name

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%09stations(name%3A%20%22complex%22)%20%7B%0A%20%20%20%20gtfsId%0A%20%20%20%20name%0A%20%20%20%20lat%0A%20%20%20%20lon%0A%20%20%20%20stops%20%7B%0A%20%20%20%20%20%20gtfsId%0A%20%20%20%20%20%20name%0A%20%20%20%20%20%20code%0A%20%20%20%20%20%20platformCode%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D) to run the query below in GraphiQL.

```graphql
{
  stations(name: "Pantelimon") {
    gtfsId
    name
    lat
    lon
    stops {
      gtfsId
      name
      code
      platformCode
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query stops by location and radius

* If the argument `first` is not used in the query, all results will be on one page.
* **Note:** argument `radius` is the maximum walking distance along streets and paths to the stop


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%20%20%7B%0A%20%20%20%20stopsByRadius(lat%3A60.199%2Clon%3A24.938%2Cradius%3A500)%20%7B%0A%20%20%20%20%20%20edges%20%7B%0A%20%20%20%20%20%20%20%20node%20%7B%0A%20%20%20%20%20%20%20%20%20%20stop%20%7B%20%0A%20%20%20%20%20%20%20%20%20%20%20%20gtfsId%20%0A%20%20%20%20%20%20%20%20%20%20%20%20name%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20distance%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D) to run the query below in GraphiQL.

```graphql
{
  stopsByRadius(lat:60.199, lon:24.938, radius:500) {
    edges {
      node {
        stop {
          gtfsId
          name
        }
        distance
      }
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query scheduled departure and arrival times of a stop

* Value `serviceDay` in the response is Unix timestamp (local timezone) of the departure date
* Values `scheduledArrival`, `realtimeArrival`, `scheduledDeparture` and `realtimeDeparture` in the response are seconds since midnight of the departure date
  * To get Unix timestamp (UTC time) of arrivals and departures, add these values to `serviceDay`

#### Next departures and arrivals

1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20stop(id%3A%20%22stpt%3A2987%22)%20%7B%0A%20%20%20%20name%0A%20%20%20%20%20%20stoptimesWithoutPatterns%20%7B%0A%20%20%20%20%20%20scheduledArrival%0A%20%20%20%20%20%20realtimeArrival%0A%20%20%20%20%20%20arrivalDelay%0A%20%20%20%20%20%20scheduledDeparture%0A%20%20%20%20%20%20realtimeDeparture%0A%20%20%20%20%20%20departureDelay%0A%20%20%20%20%20%20realtime%0A%20%20%20%20%20%20realtimeState%0A%20%20%20%20%20%20serviceDay%0A%20%20%20%20%20%20headsign%0A%20%20%20%20%7D%0A%20%20%7D%20%20%0A%7D%0A%0A%0A) to run the query below in GraphiQL.

```graphql
{
  stop(id: "stpt:2987") {
    name
      stoptimesWithoutPatterns {
      scheduledArrival
      realtimeArrival
      arrivalDelay
      scheduledDeparture
      realtimeDeparture
      departureDelay
      realtime
      realtimeState
      serviceDay
      headsign
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

#### Departures and arrivals at specific time

* Use argument `startTime` in stoptimes query
  * `startTime` is Unix timestamp (UTC timezone) in seconds


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20stop(id%3A%20%22stpt%3A2987%22)%20%7B%0A%20%20%20%20name%0A%20%20%20%20stoptimesWithoutPatterns(startTime%3A%201528633800)%20%7B%0A%20%20%20%20%20%20scheduledArrival%0A%20%20%20%20%20%20realtimeArrival%0A%20%20%20%20%20%20arrivalDelay%0A%20%20%20%20%20%20scheduledDeparture%0A%20%20%20%20%20%20realtimeDeparture%0A%20%20%20%20%20%20departureDelay%0A%20%20%20%20%20%20realtime%0A%20%20%20%20%20%20realtimeState%0A%20%20%20%20%20%20serviceDay%0A%20%20%20%20%20%20headsign%0A%20%20%20%20%7D%0A%20%20%7D%20%20%0A%7D%0A%0A%0A) to run the query below in GraphiQL.

```graphql
{
  stop(id: "stpt:2987") {
    name
    stoptimesWithoutPatterns(startTime: 1528633800) {
      scheduledArrival
      realtimeArrival
      arrivalDelay
      scheduledDeparture
      realtimeDeparture
      departureDelay
      realtime
      realtimeState
      serviceDay
      headsign
    }
  }
}
```

2. Change argument `startTime`.
3. Press play in GraphiQL to execute the query.

### Query arrivals and departures from a station

* Field `platformCode` contains the platform code used by the vehicle


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%7B%0A%20%20station(id%3A%20%22stpt%3A1000202%22)%20%7B%0A%20%20%20%20name%0A%20%20%20%20stoptimesWithoutPatterns(numberOfDepartures%3A%2010)%20%7B%0A%20%20%20%20%20%20stop%20%7B%0A%20%20%20%20%20%20%20%20platformCode%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20serviceDay%0A%20%20%20%20%20%20scheduledArrival%0A%20%20%20%20%20%20scheduledDeparture%0A%20%20%20%20%20%20trip%20%7B%0A%20%20%20%20%20%20%20%20route%20%7B%0A%20%20%20%20%20%20%20%20%20%20shortName%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20headsign%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A) to run the query below in GraphiQL.

```graphql
{
  station(id: "stpt:1000202") {
    name
    stoptimesWithoutPatterns(numberOfDepartures: 10) {
      stop {
        platformCode
      }
      serviceDay
      scheduledArrival
      scheduledDeparture
      trip {
        route {
          shortName
        }
      }
      headsign
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query departures near a specific location

* Query type **nearest** can be used to query departure rows near a specific location
* Departure row is a special location type, which lists departures of a certain route pattern from a certain stop
* Querying nearest departure rows returns only one stop per pattern
* i.e. if there are multiple stops that a certain pattern uses, only the closest stop is returned


1. Click [this link](https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2509nearest%28lat%253A%252045.7410432%252C%2520lon%253A%252021.14655%252C%2520maxDistance%253A%2520500%252C%2520filterByPlaceTypes%253A%2520DEPARTURE_ROW%29%2520%257B%250A%2520%2520%2520%2520edges%2520%257B%250A%2520%2520%2520%2520%2520%2520node%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520place%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520...on%2520DepartureRow%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stop%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lat%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520lon%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520stoptimes%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520serviceDay%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520scheduledDeparture%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520realtimeDeparture%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520trip%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520route%2520%257B%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520shortName%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520longName%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520headsign%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%2520%2520%2509distance%250A%2520%2520%2520%2520%2520%2520%257D%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D%250A%2509) to run the query below in GraphiQL.

```graphql
{
	nearest(lat: 45.7410432, lon: 21.14655, maxDistance: 500, filterByPlaceTypes: DEPARTURE_ROW) {
    edges {
      node {
        place {
          ...on DepartureRow {
            stop {
              lat
              lon
              name
            }
            stoptimes {
              serviceDay
              scheduledDeparture
              realtimeDeparture
              trip {
                route {
                  shortName
                  longName
                }
              }
              headsign
            }
          }
        }
      	distance
      }
    }
  }
}
	
```

2. Press play in GraphiQL to execute the query.
