---
title: Routes
order: 90
---

**If you are not yet familiar with [GraphQL](../0-graphql) and [GraphiQL](../1-graphiql) it is highly recommended to review those pages at first.**

## Glossary

| Term    | Explanation                                                                                                                                                                                                                                                                                                                                                                          |
| ------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Route   | A public transport service shown to customers under a single name, usually from point A to B and back. For example: trams 1 and 1A, buses 18 and 102T, or train A.<br/>Commonly used synonym: line                                                                                                                                                                                   |
| Pattern | A sequence of stops as used by a specific direction (i.e. inbound or outbound journey) and variant of a route.<br/>For example, a variant of a route could be a tram entering service from the depot and joining at the middle of the route or a route might have a short term diversion without changing the route name (longer diversions are usually marked as different routes). |
| Trip    | A specific occurance of a pattern, usually identified by the route and exact departure time from the first stop.<br/>For example: bus 102 leaving from Otaniemi on 2017-11-21 at 10:00, or more generally leaving from Otaniemi at 10:00 on specified days.                                                                                                                          |

## Query examples

**Note:** For more details about the query types **routes** and **pattern** and their parameters you can use the **Documentation Explorer** provided in GraphiQL.

**Note:** If the examples provided with an id or other parameter do not return what is expected then the value in question may not be in use any more and you should try again with an existing value.

### Query all routes where name starts with "10"

1. Click [this link](<https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520routes%28name%253A%2520%2522M42%2522%29%2520%257B%250A%2520%2520%2520%2520gtfsId%250A%2520%2520%2520%2520shortName%250A%2520%2520%2520%2520longName%250A%2520%2520%2520%2520mode%250A%2520%2520%257D%250A%257D>) to run the query below in GraphiQL.

```graphql
{
  routes(name: "M42") {
    gtfsId
    shortName
    longName
    mode
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query all bus routes where name starts with "M14"

1. Click [this link](<https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520routes%28name%253A%2520%2522M14%2522%252C%2520transportModes%253A%2520BUS%29%2520%257B%250A%2520%2520%2520%2520gtfsId%250A%2520%2520%2520%2520shortName%250A%2520%2520%2520%2520longName%250A%2520%2520%2520%2520mode%250A%2520%2520%257D%250A%257D%250A>) to run the query below in GraphiQL.

```graphql
{
  routes(name: "M14", transportModes: BUS) {
    gtfsId
    shortName
    longName
    mode
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query all tram routes where name starts with "Tv1"

1. Click [this link](<https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520routes%28name%253A%2520%2522Tv1%2522%252C%2520transportModes%253A%2520TRAM%29%2520%257B%250A%2520%2520%2520%2520gtfsId%250A%2520%2520%2520%2520shortName%250A%2520%2520%2520%2520longName%250A%2520%2520%2520%2520mode%250A%2520%2520%257D%250A%257D%250A>) to run the query below in GraphiQL.

```graphql
{
  routes(name: "Tv1", transportModes: TRAM) {
    gtfsId
    shortName
    longName
    mode
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query patterns of a route

1. Click [this link](<https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520routes%28name%253A%2520%2522Tb14%2522%29%2520%257B%250A%2520%2520%2520%2520shortName%250A%2520%2520%2520%2520longName%250A%2520%2520%2520%2520patterns%2520%257B%250A%2520%2520%2520%2520%2520%2520code%250A%2520%2520%2520%2520%2520%2520directionId%250A%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%2520%2520headsign%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D>) to run the query below in GraphiQL.

```graphql
{
  routes(name: "Tb14") {
    shortName
    longName
    patterns {
      code
      directionId
      name
      headsign
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

Example response:

```json
{
  "data": {
    "routes": [
      {
        "shortName": "Tb14",
        "longName": "Troleibuz 14",
        "patterns": [
          {
            "code": "stpt:r1006:1:01",
            "directionId": 1,
            "name": "Tb14 to Gheorghe Barițiu (stpt:s2671)",
            "headsign": "Gheorghe Barițiu"
          },
          {
            "code": "stpt:r1006:0:01",
            "directionId": 0,
            "name": "Tb14 to I.I. de la Brad (stpt:s2793)",
            "headsign": "I.I. de la Brad"
          }
        ]
      }
    ]
  }
}
```

### Query stop names by pattern ID

- See previous example on how to find pattern IDs for a route
  - Pattern ID is value of `code` in a pattern object

1. Click [this link](<https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520pattern%28id%253A%2520%2522stpt%253Ar990%253A0%253A01%2522%29%2520%257B%250A%2520%2520%2520%2520name%250A%2520%2520%2520%2520stops%2520%257B%250A%2520%2520%2520%2520%2520%2520name%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D>) to run the query below in GraphiQL.

```graphql
{
  pattern(id: "stpt:r990:0:01") {
    name
    stops {
      name
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### Query trips of a specific pattern

1. Click [this link](<https://api.opentransport.ro/graphiql/romania?query=%257B%250A%2520%2520pattern%28id%253A%2520%2522stpt%253Ar990%253A0%253A01%2522%29%2520%257B%250A%2520%2520%2520%2520code%250A%2520%2520%2520%2520directionId%250A%2520%2520%2520%2520name%250A%2520%2520%2520%2520headsign%250A%2520%2520%2520%2520trips%2520%257B%250A%2520%2520%2520%2520%2520%2520gtfsId%250A%2520%2520%2520%2520%2520%2520tripHeadsign%250A%2520%2520%2520%2520%2520%2520routeShortName%250A%2520%2520%2520%2520%2520%2520directionId%250A%2520%2520%2520%2520%257D%250A%2520%2520%257D%250A%257D>) to run the query below in GraphiQL.

```graphql
{
  pattern(id: "stpt:r990:0:01") {
    code
    directionId
    name
    headsign
    trips {
      gtfsId
      tripHeadsign
      routeShortName
      directionId
    }
  }
}
```

2. Press play in GraphiQL to execute the query.

### <a name="fuzzytrip"></a>Query a trip without its id

- Query type **fuzzyTrip** can be used to query a trip without its id, if other details uniquely identifying the trip are available
  - This query is mostly useful for getting additional details for vehicle positions received from [the vehicle position API](../../4-realtime-api/vehicle-positions/)

For example, from the following vehicle position message

```json 
{
  "VP": {
    "desi":"550",
    "dir":"1",
    "oper":12,
    "veh":1306,
    "tst":"2019-06-28T09:49:01.457Z",
    "tsi":1561715341,
    "spd":12.29,
    "hdg":47,
    "lat":60.182376,
    "long":24.825781,
    "acc":0.44,
    "dl":-2,
    "odo":24627,
    "drst":0,
    "oday":"2019-06-28",
    "jrn":99,
    "line":261,
    "start":"11:57",
    "loc":"GPS",
    "stop":null,
    "route":"2550",
    "occu":0
  }
}
```

it is possible to parse:

- Route id from the message: _2550_
- Direction id from the topic: _1_
- Departure time from the message: _11:57_
- Departure date from the message: _2019-06-28_

**Note:**

1. Vehicle position messages use different direction id than the Routing API
   - Direction id _1_ in a vehicle position is same as direction id _0_ in the Routing API
   - Direction id _2_ in a vehicle position is same as direction id _1_ in the Routing API
2. Departure time must be in seconds
   - e.g. _11:57_ = `11 * 60 * 60 + 57 * 60` = _43020_
   - If the date in fields `oday` and `tst` is not the same and the departure time (`start`) is earlier than the time in `tst`, add 86400 seconds to departure time
        - This is due to differences in time formats, when vehicles which have departed after midnight have the previous date as operating day
        - e.g.
          - `tst = 2018-08-16T00:15:00.836Z` *(note that this is in UTC time)*
          - `oday = 2018-08-15`
          - `start = 03:10`
          - → _03:10_ = `3 * 60 * 60 + 10 * 60 + 86400` = _97800_
3. Due to a bug in the vehicle position API, some route ids don't match the route id in the routing API
   - In this case, **fuzzyTrip** query returns `null`

For example, the following query checks if the vehicle, which sent the vehicle position message above, is wheelchair accessible:

```graphql
{
  fuzzyTrip(route: "stpt:r1558", direction: 0, date: "2019-06-28", time: 43020) {
    route {
      shortName
    }
    wheelchairAccessible
  }
}
```
