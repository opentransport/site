---
title: Address search
replit:
  embeds:
    -
      title: "Address search"
      url: https://repl.it/@digitransit/GeocodingAddressSearch
      height: 950px
      description: 1. Press run<br/>2. Write an address and press Enter to see the list of locations returned by the API on the map
---

Address search can be used to search addresses and points of interest (POIs).  An address is matched to its corresponding geographic coordinates and in the simplest search, you can provide only one parameter, the text you want to match in any part of the location details.

## Endpoint

`http://api.opentransport.ro/geocoding/v1/search`

### Supported URL parameters

| Parameter              | Type                   | Description                                              |
|------------------------|------------------------|----------------------------------------------------------|
| `text`                   | string                 | Text to be searched
| `size`                   | integer                | Limits the number of results returned
| `boundary.rect.min_lon`<br/>`boundary.rect.max_lon`<br/>`boundary.rect.min_lat`<br/>`boundary.rect.max_lat`	 | floating point number  | Searches using a  boundary that is specified by a rectangle with latitude and longitude coordinates for two diagonals of the bounding box (the minimum and the maximum latitude, longitude).
| `boundary.circle.lat`<br/>`boundary.circle.lon`<br/>`boundary.circle.radius` | floating point number  | Searches using location coordinates and a maximum distance radius within which acceptable results can be located.
| `focus.point.lat`<br/>`focus.point.lon` | floating point number  | Scores the nearby places higher depending on how close they are to the focus point so that places with higher scores will appear higher in the results list.
| `sources`                | comma-delimited string array | Filters results by source (value can be `oa` ([OpenAddresses](https://openaddresses.io/)), `osm` ([OpenStreetMap](http://openstreetmap.org/)))
| `layers`                 | string                 | Filters results by layer (see list of possible values [here](https://github.com/pelias/documentation/blob/master/search.md#filter-by-data-type), commonly used values are `address`, `venue` and `street`)
| `lang`                   | string                 | Returns results in the preferred language if such a language-bound name version is available (value can be `ro` or `en`).

**Note**: parameter `boundary.country` is not used by OpenTransport, as only data from Romania is available.

## Response fields

| Name              | Type    | Description                                              |
|-------------------|---------|----------------------------------------------------------|
| `id`                | string  |
| `gid`               | string  | Global id that consists of a layer (such as address or country), an identifier for the original data source (such as openstreetmap or openaddresses), and an id for the individual record corresponding to the original source identifier, where possible.
| `layer`             | string  | Place type (e.g. `address`), see list of possible values [here](https://github.com/pelias/documentation/blob/master/search.md#filter-by-data-type)
| `source`            | string  | Data source, for example `openstreetmap`, `openaddresses`
| `source_id`         | string  |
| `name`              | string  | A short description of the location, for example a business name, a locality name, or part of an address, depending on what is being searched for and what is returned.
| `postalcode`        | number  |
| `postalcode_gid`    | string  |
| `confidence`        | number  | An estimation (as a percentage) of how accurately this result matches the query
| `distance`          | number  | A distance from the query point (in kilometers)
| `accuracy`          | string  | Returns always coordinates of just one point. If the object is originally an area or a line like a road, then the centroid is calculated (value can be point or centroid).
| `country`           | string  | Places that issue passports, nations, nation-states
| `country_gid`       | string  |
| `country_a`         | string  | [ISO 3166-1 alpha-3 country code](https://en.wikipedia.org/wiki/ISO_3166-1), for example *ROU*
| `region`            | string  | For example *Banat*
| `region_gid`        | string  |
| `localadmin`        | string  | Local administrative boundaries, for example *Timisoara*
| `localadmin_gid`    | string  |
| `locality`          | string  | Towns, hamlets, cities, for example *Timisoara*
| `locality_gid`      | string  |
| `neighbourhood`     | string  | Social communities, neighbourhoods, for example *Itä-Pasila*
| `neighbourhood_gid` | string  |
| `label`             | string  | A human-friendly representation of the place with the most complete details, that is ready to be displayed to an end user, for example *East-West Pub, Iulius Town, Timisoara*.
| `bbox`              | string  | If present, it describes the geographic extent of the feature, such as the screen size necessary to show all of California without needing to send the precise polygon geometry.

**Note:** Not exactly the same fields are returned for all searches because all object locations do not have the same data available, for example neighborhood is not in use with all objects.

## Search examples

### Search for 'iuliu' and return only one result

> https://api.opentransport.ro/geocoding/v1/search?text=iuliu&size=1

**Note:** Using parameter **size=1** limits the number of results returned to one.

### Search for 'iuliu' and filter results by street address

> https://api.opentransport.ro/geocoding/v1/search?text=iuliu&layers=address

**Note:** Using parameter **layers=address** returns results for places having text kamppi with a street address.

### Search for 'iuliu' using a rectangle

> https://api.opentransport.ro/geocoding/v1/search?text=iuliu&boundary.rect.min_lat=40&boundary.rect.max_lat=50&boundary.rect.min_lon=24.3&boundary.rect.max_lon=25.5

### Search for 'iuliu' inside a circle

> https://api.opentransport.ro/geocoding/v1/search?text=iuliu&boundary.circle.lat=45.2&boundary.circle.lon=25.0&boundary.circle.radi936us=30

**Note:** Parameter **boundary.circle.radius**  is always specified in kilometers.

### Search for 'iuliu' using a focus point

> https://api.opentransport.ro/geocoding/v1/search?text=iuliu&focus.point.lat=45.2&focus.point.lon=24.936

**Note:** Using parameter **focus.point** scores nearby places higher depending on how close they are to the focus point so that places with higher scores will appear higher in the results list. After all the nearby results have been found, additional results will come from the rest of the world, without any further location-based prioritization.

## Language preference

The language preference can be defined using `lang=xx` parameter, default being `lang=ro`. Unlike in reverse
geocoding, the preference has significance for geocoding searches only when multiple languages provide
an equally good match. An example:

> https://api.opentransport.ro/geocoding/v1/search?text=finlandia&lang=en&size=1

> https://api.opentransport.ro/geocoding/v1/search?text=finlandia&lang=ro&size=1

Both match the search string `finlandia` equally well.

In most cases, an identified best match defines the language for the response, overruling the preference. An example:

> https://api.opentransport.ro/geocoding/v1/search?text=banat&lang=ro

In this case, the search string matches perfectly a romanian county name, and consiquently the result is
"Banat county, Timis". In other words, the geocoding API does not act like a translation service.

**Note:** Part of the provided geocoding data does not include Romanian names, and part of the data
leaves the language context unknown. This may occasionally cause unexpected errors in language selection.
