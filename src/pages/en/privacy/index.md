---
title: Privacy Policy
panels:
  - title: Produce your own OpenTransport services
    body: You can start you own OpenTransport service platform by configuring an modifying existing OpenTransport Docker containers and open source code repositories.
    links: []
    image: "../cloud.png"
    id: maintainers
    textColor: "#000"
    swapped: true
---

# OPEN TRANSPORT PRIVACY POLICY

Open Transport Routing S.R.L. (referred as “We”, “Us”, “Our”) has developed a mobile application (“TransTim”, “App”) that collects data from your device in order to provide you with pertinent transport information for nearby services.

## 1. CONSENT

By using “TransTim”, you accept the terms and conditions described in this Privacy Policy.

## 2. PURPOSE

This Privacy Policy describes how we collect, use and disclose your information and how this information can be accessed and deleted when necessary.

## 3. SCOPE

“TransTim” may contain links to websites, apps, products or services that are operated by third parties. This Privacy Policy does not extend to these third parties on which we have no control and for which we cannot be held liable. We encourage you to read about the privacy policies, procedures and practices of those third parties.

## 4. WHAT INFORMATION WE COLLECT FROM YOU

We collect and process information from your device in several ways, including:

- Once you have given us your consent to track your location, we keep track of your location and movement, including your latitude, longitude, time and speed of travel.
- The collection of location data only happens while “TransTim” is in the foreground (unless explicitly stated otherwise by “TransTim”) and if you’ve granted “TransTim” access to your device’s location services.
- We also automatically collect usage details each time you use “TransTim”, including the country of origin, the searches you make, the routes you interact with, the type of phone or OS you are using, etc.
- We collect contact information (email and/or push notification token) when you explicitly request to be notified when “TransTim” has new features.

## 5. HOW WE USE YOUR INFORMATION

We may use information collected from or provided by you for the following purposes:

- Provide you with pertinent transport information for nearby services;
- Understand your preferences so that we can customize “TransTim” for you, such as recommending personalized transport options;
- Measure usage of “TransTim”, including usage of specific features and views;
- Generate real-time location data of transit vehicles and broadcast the vehicle location and predicted arrival times to other users;
- Develop and improve “TransTim” based on your feedback and interactions;
- Guide product development and build new features;
- Conduct research on users’ habits and how they use public transportation;
- Identify and correct bugs and other problems with “TransTim”;
- Respond to your emails and help with support queries;
- Subject to your consent, keep you posted from time to time on latest announcements and additional information;
- Comply with law and regulations and lawful requests or orders.

## 6. USERS’ OPTIONS

The following are some examples of your options regarding collection, use and disclosure of your information:

- You can stop all collection of location data easily and at any time by disabling location services in your device’s settings or by uninstalling “TransTim”.

## 7. DISCLOSURE TO THIRD PARTIES

We do not disclose your information to third parties without your explicit consent except for the following purposes:

- In case you request assistance with the use of “TransTim”, we might share such request, including your contact information, with a third party directly involved in the issue (i.e. if you inform us over email that certain transit schedules in the app are outdated, and we forward this message to the applicable transit agency).
- When you purchase or access services from a third party (e.g. a mobility operator) through our app, we will share the information required to ascertain your lawful use of those services (including tokenized payment information, relevant personally identifiable information, and any relevant location data). For example, if you purchase a public transit ticket, we may share the time and location where you purchased that ticket in order to ensure that no fraudulent tickets have been sold; or if you try to unlock a bikeshare bike, we may verify your location to ensure that you’re near the station before you unlock a bike.
- In case of a prospective business transaction, you agree that “TransTim” may disclose anonymous and aggregate information to another corporation for the parties to determine whether to proceed with such transaction. In case a business transaction is completed, you agree that any information may be transferred to the corporation that owns “TransTim” or the relevant part of it in order to carry its business. A “business transaction” includes, but is not limited to, a reorganization, merger, purchase, sale or other acquisition or disposition of a corporation or any of its assets.
- In case we are required to comply with applicable law and regulations and lawful requests or orders or if it is otherwise permitted by law (i.e., protect and defend our rights, situation that threatens life, health or safety, etc.).

We also disclose anonymous and aggregate information to certain third parties for research, product development or other business purposes. For instance, we may share information with transit agencies to help them analyze and improve their services.

### 7.1. THIRD PARTY SOFTWARE/SERVICE

While using “TransTim”, we may be using third party software and/or service for various needs, inter alia, in order to collect and/or process the information detailed herein (the "Third Party Service Provider"). Note that these are independent software and/or service providers and we do not take any liability about the information collection policies of these providers, or about any kind of issue, legal or otherwise, relating thereto.

We may also provide additional transit-related services, including on-demand transit services, provided by our third-party operational service partners (as they may evolve from time to time at our discretion). Such third-party providers are subject to a respective term of use and privacy policies.

## 8. SECURITY SAFEGUARDS

We are committed to securing your information and we take reasonable physical, organizational and technical measures to protect your information against unauthorized access, use, disclosure, modification or destruction.

These safeguards include, but are not limited to:

- Storage on secure servers;
- Encryption of the communication channels;
- Limited access to employees and contractors on a “needs-to-know” basis.

Although we take great efforts to protect your information, no security system can prevent all potential security breaches nor be immune from any wrongdoings or malfunctions.

## 9. RETENTION PERIOD

We will retain your information as long as you use “TransTim” and for a reasonable time thereafter.

## 10. RIGHT TO ACCESS AND REMOVAL

We store your information on servers located in Romania and Germany.

If you would like to remove or access any information that we have collected through “TransTim” that is stored on our servers, please contact us at privacy@opentransport.ro. On receiving such a request, we will use reasonable efforts to provide you or delete such information.

## 11. CHILDREN

We do not knowingly collect, use or disclose information from children under the age of 13. If we learn that we have collected information of a child under 13 without first receiving parental consent, we will take steps to delete such information within a reasonable time. If you have reason to believe that a child under the age of 13 has provided information to us, please contact us and we will take steps to delete such information within a reasonable time.

## 12. DO NOT TRACK SIGNALS

“TransTim” does not respond to Web browser “Do Not Track” (DNT) signals or other similar mechanisms.

Third parties do not collect information about your online activities over time and across different Web sites when using “TransTim”.

## 13. RIGHTS OF EUROPEAN DATA SUBJECTS (GDPR)

By using “TransTim” or communicating with us, you consent to the purposes for which we process your personal data. We may process your personal data if we have other lawful grounds to do so.

You have a right to:

- Withdraw consent to processing, where consent is the basis of processing.
- Access your personal data that we hold and how to process it, under certain conditions.
- Demand rectification of inaccurate personal data about you.
- Object to unlawful data processing under certain conditions.
- Require erasure of past data about you (your "right to be forgotten") under certain conditions.
- Demand that we restrict processing of your personal data, under certain conditions, if you believe: we have exceeded the legitimate basis for processing, processing is no longer necessary, your personal data is inaccurate.
- Port personal data concerning you that you provided us in a structured, commonly used, and machine-readable format, subject to certain conditions.
- Complain to a data protection supervisory authority in your country.

The personal data we collect is not used for automated decision making and profiling, except for automated processes in the context of marketing. As stated above, you can opt-out of our direct marketing under certain conditions.

To learn more about your rights under the GDPR you can visit the European Commission’s page on Protection of Personal Data.

## 14. UPDATE

We reserve the right to amend or change this Privacy Policy at any time without notice. In case of substantial changes to this Privacy Policy, we will alert you through our social media accounts within 30 days before such changes enter into effect. We will also update the privacy policy within the app on the date that such changes become effective. By using “TransTim”, you agree to be bound by the version of the privacy policy displayed on the date upon which you use the app. If you do not accept the amended or changed Privacy Policy, you may not access, use or continue to use “TransTim”.

By continuing to access and use “TransTim”, you are acknowledging your acceptance of the amendments and changes. In any case, we recommend that you consult this page regularly in order to be informed of any changes in this Privacy Policy.

When we post changes to this Privacy Policy, we will revise the "last updated" date at the end of this Privacy Policy.

## 15. CONTACT INFORMATION

If you have any questions or concerns about this Privacy Policy, please contact us by email at privacy@opentransport.ro and we will examine and reply to all communications within reasonable delays.

This Privacy Policy was last updated on October 29, 2019.
