---
title: Donate
panels:
  - title: DONATE 🍻
    body: If you find our initiative to get cities moving interesting, help us by donating something. The supported donation methods are presented bellow.
    links: []
    image: "../../donate.svg"
    id: team
    textColor: "#000"
    swapped: true
---

## VIA PAYPAL

PayPal is an electronic commerce (e-commerce) company that facilitates payments between parties through online funds transfers.

Money can be transferred for free internationally if both parties have PayPal accounts. This makes PayPal a competitive force in the world of bank transfers when sending U.S. currency internationally. When foreign currency is accepted into another country, PayPal's fees become more comparable to that of a traditional bank.

<div style="text-align:center">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="YZ4CZ8QPWMU6A">
        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button">
        <img alt="" src="https://www.paypal.com/en_RO/i/scr/pixel.gif" width="1" height="1">
    </form>
</div>