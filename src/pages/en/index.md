---
headerText: "The next generation journey planner. Contribute and leave your mark!"
isFront: true
panels:
  - title: For users
    body: You are the focus, always. The next generation journey planner pinpoints your location and shows nearby routes, stops and timetables, in real time! Real time means that you will see the location of buses and trains, as well as the accurate times of arrival at the stops. No more time wasted waiting. The service filters unnecessary information and tells what is going on around you and how to get to your destination more conveniently. In the future, the real time service will cover the whole country.
    links:
      - title: Go to the national gomee.ro service
        url: http://gomee.ro/
    image: "../users.svg"
    id: users
    background: "#ff8a3c"
    textColor: "#ffffff"
    swapped: true
  # - title: For developers
  #   body: Join us in developing the next generation journey planner that is used by hundreds of thousands of people every day. Probably by you, too. You can develop the service further as a whole or improve just one part of it. Make use of the code, create something new, and show it to others! You’ll be using state-of-the-art browser technology and will soon become familiar with the development environment. Roll up your sleeves and download OpenTransport. The code is open-source.
  #   links:
  #     - title: Read the platform architecture description
  #       url: /en/developers/architecture/
  #     - title: Read more about the interfaces
  #       url: /en/developers/apis/
  #     # - title: Quick start guide
  #   image: "../developers.svg"
  #   id: developers
  #   background: "#000000"
  #   textColor: "#ffffff"
  - title: For municipalities
    body: Be on the map and make your home municipality easier to move around. Join us in developing the next generation journey planner and get national visibility for your home municipality. OpenTransport is an easy-to-access service platform provided by OpenTransport. Thanks to its open-source nature, all interested parties can participate in the development of the service. This is likely to result in better quality, improve security and provide data that is always up-to-date. Make sure that the route and timetable information for your municipality are available for the service platform.
    links: []
      # - title: OpenTransport palvelukuvaus
      # - title: Liity palveluun
    image: "../municipalities.svg"
    id: municipalities
    textColor: "#000000"
    background: "#ffffff"
    swapped: true
  - title: For service maintainers
    body: Set up your own routing services using Docker containers and open source repositories from OpenTransport.
    links:
      - title: Hosting OpenTransport services
        url: /en/services/
    image: "../cloud.svg"
    id: maintainers
    background: "#ff3c3ca8"
    textColor: "#ffffff"
    swapped: false
---
<div style="text-align: center;">

### Development plan
<span class="large-link">[Roadmap of upcoming features (in Romanian) »](../roadmap/)</span>
</div>
