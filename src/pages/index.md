---
headerText: "Planificarea călătoriilor mai ușoară ca niciodată. Contribuie și lasă-ți amprenta!"
isFront: true
downloadUrl: "https://play.google.com/store/apps/details?id=com.claug.timisoaratransport"
panels:
  - title: Pentru utilizatori
    body: Tu ești în centrul atenției, întotdeauna. Planificatorul de călătorii  vă identifică locația și afișează rutele, stațiile și programele din apropiere, în timp real! "Timp real" înseamnă că veți vedea locația autobuzelor, tramvaielor, troleibuzelor precum și orele exacte de sosire în stații. Nu mai pierdeți timpul în așteptare. Serviciul filtrează informațiile inutile și spune ce se întâmplă în jurul tău și cum să ajungi la destinație mai convenabil. În viitor, ne dorim ca serviciul să acopere întreaga țară.
    links:
      - title: Accesați serviciul regional timisoara - tm.opentransport.ro
        url: http://tm.opentransport.ro/
      - title: Seturi date pentru baza de date a adreselor
        url: /adrese/
    image: "./users.svg"
    id: users
    background: "#ff8a3c"
    textColor: "#ffffff"
    swapped: true

  # - title: Pentru programatori
  #   body: Alăturați-va pentru a dezvolta planificatorul de călătorii care este folosit de sute de mii de oameni în fiecare zi,probabil chiar și de tine. Puteți dezvolta serviciul în continuare în ansamblu sau puteți îmbunătăți/dezvolta doar o parte din acesta. Folosește codul, creează ceva nou și arată-le altora ce ai reușit! Vei folosi tehnologii de ultimă generație iar  familiarizarea cu mediul de dezvoltare durează puțin. Rotiți-vă mânecile și inspectați codul, acesta este open-source.
  #   links:
  #     - title: Răsfoiți descrierea arhitecturii sistemului
  #       url: /en/developers/architecture/
  #     - title: Citiți mai multe despre API (Application Programming Interface)
  #       url: /en/developers/apis/
  #     # - title: Quick start guide
  #   image: "./developers.svg"
  #   id: developers
  #   background: "#000000"
  #   textColor: "#ffffff"

  - title: Pentru municipalități
    body: Fiți pe hartă și faceți mai ușoară mișcarea persoanelor in orasul in care trăiți. Alăturați-ne pentru a dezvolta planificatorul de călătorii și pentru a obține vizibilitate națională pentru orașul dumneavoastră. OpenTransport este o platformă de servicii ușor de accesat. Datorită naturii sale open source, pot participa la toate părțile interesate pentru dezvoltarea serviciului. Acest lucru poate duce la o calitate mai bună, o securitate îmbunătățită și seturi de date care sunt mereu la zi. Asigurați-vă că ruta și informațiile despre orarul transportului public pentru municipalitatea dvs. sunt disponibile pentru platforma de servicii.
    links:
      - title: Contactează-ne pentru o colaborare
        url: /colaborare/
    image: "./municipalities.svg"
    id: municipalities
    textColor: "#000000"
    background: "#ffffff"
    swapped: true

  - title: Pentru cei ce oferă servicii
    body: Configurați-vă propriile servicii de rutare utilizând containere Docker și date open source de la OpenTransport.
    links:
      - title: Gazduire privata a  serviciilor oferite de OpenTransport
        url: /servicii/
    image: "./cloud.svg"
    id: maintainers
    background: "#ff3c3ca8"
    textColor: "#ffffff"
    swapped: false
---
<!-- 
<div style="text-align: center;">

## OpenTransport navigator

<span class="large-link">[Timisoara »](https://tm.opentransport.ro)</span>
<span class="large-link">[Roadmap »](/roadmap/)</span>

</div> 
-->
