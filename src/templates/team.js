import React from "react";
import styled from "styled-components";
import { graphql } from "gatsby";

import FrontPagePanels from "../components/FrontPagePanels";
import Layout from "../components/Layout";
import Markdown from "../components/Markdown";
import SEO from "../components/SEO";
import PageContainer from "../components/PageContainer";

import typography from "../utils/typography";

import cludiuImage from "../pages/en/team/claudiu.jpg";
import vladImage from "../pages/en/team/vlad.jpg";
import mihaiImage from "../pages/en/team/mihai.jpg";
const { rhythm } = typography;

const JoinOpenTransport = styled.div`
  & img {
    float: right;
  }

  & h3 {
    clear: right;
  }

  @media (max-width: 600px) {
    & img {
      height: 200px;
      float: right;
    }
  }

  .employees {
    margin-top: 20px;
    overflow: auto;
    text-align:center;
  }
  
  .employee {
    width: 230px;
    display: inline-block;
    margin: 0 0 50px 0;
    text-align: center;
    padding: 0 30px;
  }
  
  @media only screen and (max-width: 700px) {
    .employee {
      width: 100%;
      display: inline-block;
      margin: 0 0 50px 0;
      text-align: center;
    }
    .employee .image img {
      clear:both;
      border-radius: 50%;
      width: 230px;
      height: 230px;
    }
  }
  
  .employee .image {
    width: auto;
    display: inline-block;
    text-align: center;
  }
  .employee .image img {
    clear:both;
    border-radius: 50%;
    width: auto;
    height: auto;
  }
  
  .employee .name {
    font-size: 18px;
    font-weight: 600;
    margin-bottom: 0;
  }
  
  .employee .position {
    font-size: 16px;
    font-weight: 400;
  }
`

export default props => {
  return (
    <>
      <SEO
        pageTitle={props.data.markdownRemark.frontmatter.title}
        pageDescription={props.data.markdownRemark.excerpt}
        pagePath={props.data.markdownRemark.fields.slug}
      />
      <Layout slug={props.data.markdownRemark.fields.slug}>
        <div>
          <div style={{ height: `calc(${rhythm(1.5)} + 23px)` }} />
          <FrontPagePanels {...props.data.markdownRemark.frontmatter} />
          <PageContainer>
            <JoinOpenTransport>
              <Markdown {...props} />
              <div class="employees">
                <div class="employee">
                    <div class="image">
                      <img src={ cludiuImage } width="230"/>
                    </div>
                    <p class="name">Claudiu Groza</p>
                    <p class="position">Kotlin Master</p>
                </div>
                <div class="employee">
                    <div class="image">
                      <img src={ vladImage } width="230"/>
                    </div>
                    <p class="name">Vlad Vesa</p>
                    <p class="position">Backend Sourcerer</p>
                </div>
                <div class="employee">
                    <div class="image">
                      <img src={ mihaiImage } width="230"/>
                    </div>
                    <p class="name">Mihai Balint</p>
                    <p class="position">Python Master</p>
                </div>
              </div>
            </JoinOpenTransport>
          </PageContainer>
        </div>
      </Layout>
    </>
  );
};

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      fields {
        slug
      }
      frontmatter {
        title
        panels {
          title
          body
          links {
            title
            url
          }
          image {
            publicURL
          }
          id
          textColor
          swapped
        }
      }
      html
      excerpt(pruneLength: 200)
    }
  }
`;
