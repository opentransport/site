FROM node:10 as build

ENV WORK=/opt/site

WORKDIR ${WORK}

RUN yarn global add gatsby-cli@2.4.5 && \
  mkdir -p ${WORK}

# Add application
ADD . ${WORK}

RUN yarn && gatsby build

FROM node:10

WORKDIR /opt/ot-site
COPY --from=build /opt/site/public ./
COPY --from=build /opt/site/serve.json ./
RUN yarn global add serve@10.1.1
EXPOSE 8080
CMD serve -l 8080
